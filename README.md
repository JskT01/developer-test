# Developer Test



## Requerimientos
Clonar este repositorio y abrir con el editor [Unity](https://unity3d.com/es/get-unity/download) 2019 o superior

## Descripción
Esta prueba consta de una escena en la cual está llevándose a cabo un incendio.
En la escena hay presente un extinguidor. El objetivo es usar el extinguidor para apagar el fuego.

## Instrucciones
La escena en la que hay que trabajar se encuentra en la ruta:
Assets\Project Assets\Scenes\Main
Los scripts realizados deberán estar localizados en la siguiente ruta:
Assets\Project Assets\Scripts\DevTest
Pueden existir carpetas adicionales dentro de esta, siempre y cuando los scripts y las carpetas adicionales esten alojados dentro de la carpeta "DevTest"

En general, el proyecto debe constar de las siguientes actividades:

1.	Por medio de un canvas de texto, dar instrucciones al usuario para indicarle que debe usar el extintor para apagar el fuego
2.	El extinguidor se deberá poder sujetar al hacer clic en el
3.	Con el movimiento del cursor del mouse, se deberá mover el extinguidor
4.	Para usar el extinguidor, se deberá permitir al usuario mantener el clic izquierdo con el mouse
5.	El extinguidor deberá expedir sustancia compuesta (humo) al ser usado
6.	El extinguidor deberá emitir sonido al ser usado
7.	Se deberá poder apagar el fuego usando la sustancia compuesta del extinguidor

## Criterio de aceptación
- Finalizar al menos el 50% de las tareas
- Comprimir las carpetas necesarias para exportar el proyecto en formato .zip y subir en la siguiente [liga](https://1drv.ms/u/s!Ar7Mh8Ez9MqnhZBfdUcq4RC1lzk1dA?e=88jLfy) o subir el proyecto a un drive y compartir la liga
- Colocar su nombre completo al nombre del archivo .zip
- Notificar al momento de completado y responder correo con la liga all drive de los archivos del proyecto
